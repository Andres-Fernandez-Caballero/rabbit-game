// VARIABLES GLOBALES
const magicHatImage = '/images/magic_hat.png'
const rabbitImage = '/images/rabit.png'
const images = document.querySelectorAll('.figure-item')
let rabbitPosition;

window.onload = function(event) {
    //selector de multiples elementos
    /* ejemplo de lo que devuelve la lista
    images = [
        <img src="imagen1.jpg" alt="imagen"/>,
        <img src="imagen2.jpg" alt="imagen"/>,
        <img src="imagen3.jpg" alt="imagen"/>,
    ]
    */
    // console.log('imagen del conejo', images[1].src);

    images.forEach(function(image){
        image.src = magicHatImage;
    })

    images.forEach(function(image, index){
        image.addEventListener('click', function(event){
            if(index === rabbitPosition){
                image.src = rabbitImage;
                alert('GANASTE 🐰🪄')
            }else{
                alert('no esta aca 🎩')
            }
        })
    })
    rabbitPosition = randombRabbitPosition(images.length)
    console.log('🐇 = ' + rabbitPosition);

}

function randombRabbitPosition(imagesSize){
    return Math.floor(Math.random() * ((imagesSize-1) - 0 + 1) + 0);
}

